<?php
defined('BASEPATH') OR exit('No direct acript access allowed');
include_once APPPATH.'libraries/component/Table.php';

class ContasModel extends CI_Model{
    
    public function __construct(){
        $this->load->library('conta', '', 'bill');
    }
        
    public function cria($tipo)
    {
        if (sizeof($_POST)==0) return;
        
        list($ano, $mes) = explode('-', $_POST['month']);
        
        $_POST['ano'] = $ano;
        $_POST['mes'] = $mes;
        
        $data = $this->input->post();
        $this->validate();
        
        if($this->form_validation->run()){
            if($data['id']){
                $this->bill->edita($data);
            }else{
                $this->bill->cria($data);

            }
        }
    }

    private function validate(){
    
        $this->form_validation->set_rules('parceiro', 'Parceiro Comercial ou credor', 'required');
        $this->form_validation->set_rules('descricao', 'Descrever o tipo de conta a pagar', 'required');
        $this->form_validation->set_rules('valor', 'Valor a ser pago', 'required|greater_than[0]');
        $this->form_validation->set_rules('mes', 'Mês que o valor foi ou deverá ser pago', 'required');
        $this->form_validation->set_rules('ano', 'Ano o qual o valor foi pago', 'required|greater_than[2019]');
    }

    public function lista($tipo, $mes, $ano)
    {
        $v = $this->bill->lista($tipo, $mes, $ano);
        $data = [];
        foreach ($v as $row) {
           
            $aux['parceiro'] = $row['parceiro'];
            $aux['descricao'] = $row['descricao'];
            $aux['valor'] = $row['valor'];
            $aux['btn'] = $this->getActionButton($row);
            $data[] = $aux;

        }
        $label = ['Parceiro', 'Descrição', 'Valor R$','Alterações'];
        
        $table = new Table($data, $label);
        return $table->getHTML();
    }

    private function getActionButton($row){
        $cor = $row['liquidada'] % 2 ? 'text-success':'text-dark';
        $html = '<a><i id="' . $row['id'] . '" class="fas fa-check-circle mr-3 '.$cor.' pay_btn"></i></a>';
        $html .= '<a><i id="' . $row['id'] . '" class="fas fa-edit mr-3 text-primary edit_btn"></i></a>';
        $html .= '<a><i id="'   . $row['id'] . '" class="fas fa-trash-alt mr-1 text-danger delete_btn"></i></a>';

        return $html;
    }
    
    public function delete_conta(){
        
        $data = $this->input->post();
        $this->bill->delete($data);
    }

    public function status_conta(){
        $data = $this->input->post();
        $this->bill->status($data);
    }
}