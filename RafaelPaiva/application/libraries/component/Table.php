<?php

class Table{
    
    private $data; 
    private $label;
    
    function __construct(array $data, array $label){
        $this->data = $data;
        $this->label = $label;
    }

    private function header(){ 
        $html = '<thead class="text-dark table-primary"><tr>';
        foreach ($this->label as $col) {
            $html .= '<th scope="col">'.$col.'</th>';
        }
        $html .= '</tr></thead>';
        return $html;
    }


    private function body(){

        $html = '<tbody class="table table-warning">'.
                $this->rows().
                '</tbody>';

        return $html;
    }

    private function rows(){
        $html = '';
        foreach ($this->data as $row) {
            $html .= '<tr>';
            
            foreach ($row as $col) {
                $html .='<td scope="col">'.$col.'</td>';
            }
            
            $html .='</tr>';
        }

        return $html;
    }
    

    public function getHTML(){
        $html = '
            <table class="table table-hover">'.
            $this->header().
            $this->body().   
            '</table>';

        return $html;
    }
}