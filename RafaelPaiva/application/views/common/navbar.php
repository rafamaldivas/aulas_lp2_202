<nav class="navbar navbar-expand-lg navbar-light bg-black ">
    <div class="container-fluid">
        <div class="collapse navbar-collapse" id="navbarExample01">
            <ul class="navbar-nav mr-auto mb-1 mr-3">
                <li class="nav-item active">
                    <a class="nav-link bg-primary text-white rounded-lg ml-2" aria-current="page" href="<?= base_url('home') ?>">Home</a>
                </li>
                <li class="nav-item active">
                    <a class="nav-link bg-success rounded-lg ml-2" aria-current="page" href="<?= base_url('Welcome/login') ?>">Login</a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle bg-success rounded-lg ml-3" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-expanded="false">
                        Cadastro
                    </a>
                    <ul class="dropdown-menu bg-success" aria-labelledby="navbarDropdownMenuLink">
                        <li><a class="dropdown-item" href="<?= base_url('Usuario/cadastro') ?>">Usuário</a></li>
                        <li><a class="dropdown-item" href="#">Conta Bancária</a></li>
                        <li><a class="dropdown-item" href="#">Parceiros</a></li>
                    </ul>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle bg-success rounded-lg ml-3" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-expanded="false">
                        Lançamentos
                    </a>
                    <ul class="dropdown-menu bg-success" aria-labelledby="navbarDropdownMenuLink">
                        <li><a class="dropdown-item" href="<?= base_url('Contas/pagar/02/2021') ?>">Contas a Pagar</a></li>
                        <li><a class="dropdown-item" href="<?= base_url('Contas/receber/02/2021') ?>">Contas a Receber</a></li>
                        <li><a class="dropdown-item" href="#">Fluxo de Caixa</a></li>
                    </ul>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle bg-success rounded-lg ml-3" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-expanded="false">
                        Relatórios
                    </a>
                    <ul class="dropdown-menu bg-success" aria-labelledby="navbarDropdownMenuLink">
                        <li><a class="dropdown-item" href="#">Lançamento por Período</a></li>
                        <li><a class="dropdown-item" href="<?= base_url('Contas/movimento') ?>">Movimento de Caixa</a></li>
                        <li><a class="dropdown-item" href="#">Resumo Anual</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</nav>