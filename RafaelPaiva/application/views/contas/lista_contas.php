<div class="container card-body">
    
    <div class="row mt-5">
        <div class="col-md-2">
            <a class="btn btn-primary" data-toggle="collapse" href="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                Criar Nova Conta
            </a>
        </div>
        <div class="col-md-2 offset-md-7 mt-3">
            <input type="month" id="month" name="month" value="<?= set_value('month') ?>">
        </div>
    </div>

    <?php echo form_error('ano', '<div class="alert alert-danger">', '</div>') ?>
    <?php echo form_error('parceiro', '<div class="alert alert-danger">', '</div>') ?>
    <?php echo form_error('descricao', '<div class="alert alert-danger">', '</div>') ?>
    <?php echo form_error('valor', '<div class="alert alert-danger">', '</div>') ?>

    <div class="collapse" id="collapseExample">
        <div class="mt-3">
            <div class="row">
                <div class="col-md-6 mx-auto mt-lg-2 d-inline p-2 border">
                    <form method="POST" id="contas-form">
                        <div class="form-outline mt-5">
                            <input type="text" name="parceiro" id="parceiro" class="form-control " value="<?= set_value('parceiro') ?>" />
                            <label class="form-label" for="form1">Devedor / Credor</label>
                        </div>
                        <div class="form-outline mt-3">
                            <input type="text" id="descricao" name="descricao" class="form-control" value="<?= set_value('descricao') ?>" />
                            <label class="form-label" for="form1">Descrição</label>
                        </div>
                        <div class="form-outline mt-3 border-4">
                            <input type="number" name="valor" id="valor" class="form-control" value="<?= set_value('valor') ?>" />
                            <label class="form-label" for="form1">valor</label>
                        </div>
                        <div class="form-check ml-2">
                            <input class="form-check-input" type="hidden" name="tipo" value="<?= $tipo ?>">
                            <input type="hidden" name="id" id="conta_id">
                        </div>
                        <br>
                        <a class="btn btn-primary mt-lg-auto" onclick="document.getElementById('contas-form').submit();">Enviar</a>

                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="row mt-3">
        <div class="col">
            <?= $lista ?>
        </div>
    </div>
</div>


<div class="modal fade" id="basicExampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Exclusão de Contas</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                Deseja Realmente Deletar essa Conta? Esta ação é irreversível!
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                <button type="button" id="deleteBtn" class="btn btn-primary">Deletar</button>
            </div>
        </div>
    </div>
</div>

<script>
    var row_id = 0;

    $(document).ready(function() {
        $('#month').change(loadMonth);
        $('.delete_btn').click(openModal);
        $('#deleteBtn').click(deleteRow);
        $('.edit_btn').click(exibeForm);
        $('.pay_btn').click(liquidaConta);
    })

    function loadMonth(){
        var data = this.value.split('-');
        var ano = data[0];
        var mes = data[1]; 
        
        var v = window.location.href.split('/');
        var url = v.slice(0,7).join('/');
        url = url + '/'+ mes + '/' + ano;   
        window.location.href = url;
    }

    function exibeForm() {
        var row_id = this.id;
        var td = $('#' + row_id).parent().parent().parent().children();
        $('#parceiro').val($(td[0]).text());
        $('#descricao').val($(td[1]).text());
        $('#valor').val($(td[2]).text());
        $('#mes').val($(td[4]).text());
        $('#ano').val($(td[5]).text());
        $('#conta_id').val(row_id);
        $('#collapseExample').collapse('show');



    }

    function openModal() {

        var row_id = this.id;
        $('#basicExampleModal').modal();

    }


    function deleteRow() {
        var id = row_id;
        $.post(api('contas', 'delete_conta'), {
            id
        }, function(d, s, x) {
            $('#' + row_id).parent().parent().parent().remove();
            $('#basicExampleModal').modal('hide');
        });
    }

    function liquidaConta() {
        var id = this.id;
        $.post(api('contas', 'status_conta'), {
                id
            },
            function(d, s, x) {
                $('#' + id).toggleClass('text-dark text-success');
            });



    }
</script>