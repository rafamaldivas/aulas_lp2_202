<div class="d-flex justify-content-center align-items-center" style="height: 100vh;">
    <div class="text-center">

        <h3 class="mb-4">Identificação de Usuários</h3>
        <form class="text-center border border-light p-5" method="POST">

            <p class="h4 mb-4">Entre</p>

            <input type="email" id="email" name="email" class="form-control mb-4" placeholder="E-mail">

            <input type="password" id="senha" name="senha" class="form-control mb-4" placeholder="Senha">

            <button class="btn btn-info btn-block my-4" type="submit">Enviar</button>
            
            <p class="btn-danger"><?= $error ?'Dados de Acesso Incorretos' : ''?></p>
        </form>
    
    </div>
</div>